Source: ktorrent
Section: net
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 5.91~),
               gettext,
               kross-dev (>= 5.82~),
               libboost-dev (>= 1.71.0~),
               libgeoip-dev,
               libkf5archive-dev (>= 5.91~),
               libkf5completion-dev (>= 5.91~),
               libkf5config-dev (>= 5.91~),
               libkf5configwidgets-dev (>= 5.91~),
               libkf5coreaddons-dev (>= 5.91~),
               libkf5crash-dev (>= 5.91~),
               libkf5dbusaddons-dev (>= 5.91~),
               libkf5dnssd-dev (>= 5.91~),
               libkf5doctools-dev (>= 5.91~),
               libkf5i18n-dev (>= 5.91~),
               libkf5iconthemes-dev (>= 5.91~),
               libkf5itemviews-dev (>= 5.91~),
               libkf5kcmutils-dev (>= 5.91~),
               libkf5kio-dev (>= 5.91~),
               libkf5notifications-dev (>= 5.91~),
               libkf5notifyconfig-dev (>= 5.91~),
               libkf5parts-dev (>= 5.91~),
               libkf5plotting-dev (>= 5.91~),
               libkf5solid-dev (>= 5.91~),
               libkf5syndication-dev (>= 1:5.91~),
               libkf5textwidgets-dev (>= 5.91~),
               libkf5widgetsaddons-dev (>= 5.91~),
               libkf5windowsystem-dev (>= 5.91~),
               libkf5xmlgui-dev (>= 5.91~),
               libktorrent-dev (>= 20.11.70~),
               libphonon4qt5-dev,
               libphonon4qt5experimental-dev,
               libtag1-dev,
               pkg-config,
               pkg-kde-tools (>= 0.15.16),
               qtbase5-dev (>= 5.15.2~),
               qtscript5-dev (>= 5.7.0),
               qtwebengine5-dev (>= 5.15.2~) [amd64 arm64 armhf i386 mips64el mipsel],
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://www.kde.org/applications/internet/ktorrent/
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/ktorrent.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/ktorrent

Package: ktorrent
Architecture: any
Depends: ktorrent-data (= ${source:Version}),
         libktorrent-l10n,
         ${misc:Depends},
         ${shlibs:Depends},
Suggests: geoip-database, krosspython
Description: BitTorrent client based on the KDE platform
 This package contains KTorrent, a BitTorrent peer-to-peer network client, that
 is based on the KDE platform. Obviously, KTorrent supports such basic features
 as downloading, uploading and seeding files on the BitTorrent network.
 However, lots of other additional features and intuitive GUI should make
 KTorrent a good choice for everyone. Some features are available as plugins
 hence you should make sure you have the ones you need enabled.
   - Support for HTTP and UDP trackers, trackerless DHT (mainline) and webseeds.
   - Alternative UI support including Web interface.
   - Torrent grouping, speed capping, various download prioritization
     capabilities on both torrent and file level as well as bandwidth
     scheduling.
   - Support for fetching torrent files from many sources including any local
     file or remote URL, RSS feeds (with filtering) or actively monitored
     directory etc.
   - Integrated and customizable torrent search on the Web.
   - Various security features like IP blocking and protocol encryption.
   - Disk space preallocation to reduce fragmentation.
   - uTorrent compatible peer exchange.
   - Autoconfiguration for LANs like Zeroconf extension and port forwarding via
     uPnP.
   - Scripting support via Kross and interprocess control via DBus interface.
   - SOCKSv4 and SOCKSv5 proxy, IPv6 support.
   - Lots of other useful built-in features and plugins.

Package: ktorrent-data
Architecture: all
Depends: ${misc:Depends}
Recommends: ktorrent (>= ${source:Version})
Description: KTorrent data and other architecture independent files
 This package contains architecture independent data files for KTorrent,
 a BitTorrent peer-to-peer network client. Unless you have ktorrent package
 installed, you probably won't find this package useful.
